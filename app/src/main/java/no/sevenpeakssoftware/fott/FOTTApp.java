package no.sevenpeakssoftware.fott;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.orm.SugarApp;
import com.orm.SugarRecord;

import no.sevenpeakssoftware.fott.model.Article;
import no.sevenpeakssoftware.fott.utils.BitmapLruCache;


public class FOTTApp extends SugarApp {
    public static final String TAG = FOTTApp.class.getSimpleName();

    private static FOTTApp sInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private BitmapLruCache mBitmapLruCache;

    public FOTTApp() {
        // not used
    }

    public static synchronized FOTTApp getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        SugarRecord.deleteAll(Article.class);
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        mRequestQueue.cancelAll(tag);
    }

    public BitmapLruCache getBitmapLruCache() {
        if (mBitmapLruCache == null) {
            mBitmapLruCache = new BitmapLruCache();
        }

        return mBitmapLruCache;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, getBitmapLruCache());
        }
        return this.mImageLoader;
    }
}
