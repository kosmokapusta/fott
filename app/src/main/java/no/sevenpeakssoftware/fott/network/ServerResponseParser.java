package no.sevenpeakssoftware.fott.network;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import no.sevenpeakssoftware.fott.model.Article;

public class ServerResponseParser {

    public static void parseAllArticlesResponse(JSONObject response) {
        // TODO: gson parsing
        try {
            JSONArray array = (JSONArray) response.get("content");
            for(int i = 0; i < array.length(); ++i) {
                JSONObject obj = array.getJSONObject(i);
                String innerId = obj.getString("id");
                String date = obj.getString("dateTime");
                int changed = obj.getInt("changed");
                String text = obj.getJSONArray("content").getJSONObject(0).getString("description");
                String subject = obj.getJSONArray("content").getJSONObject(0).getString("subject");
                String title = obj.getString("title");
                String ingress = obj.getString("ingress");
                String imageUrl = obj.getString("image");

                Article article = new Article(innerId, date, changed, title, ingress, text, subject, imageUrl);
                article.save();

                long new_count = Article.count(Article.class, null, null);
                Log.d("DB_COUNT", "Real count: " + new_count);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
