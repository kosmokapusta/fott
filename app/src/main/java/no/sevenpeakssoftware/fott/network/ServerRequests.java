package no.sevenpeakssoftware.fott.network;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import no.sevenpeakssoftware.fott.FOTTApp;

public class ServerRequests {
    private static String GET_ARTICLES_URL = "http://87.251.89.41/application/11424/article/get_articles_list ";

    public static void getAllArticlesRequest(final ServerRequestCallback callback) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(GET_ARTICLES_URL, new JSONObject(), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (callback != null) {
                    callback.onSuccess(response);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onError(error);
                }
            }
        });

        FOTTApp.getInstance().addToRequestQueue(jsObjRequest);
    }
}
