package no.sevenpeakssoftware.fott.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import no.sevenpeakssoftware.fott.R;

public class ErrorHandler {
    private static final String TAG = ErrorHandler.class.getSimpleName();

    public ErrorHandler() {
    }

    public static void showError(Context context, VolleyError error) {
        String json;
        NetworkResponse response = error.networkResponse;
        if (response != null && response.data != null) {
            json = new String(response.data);
            json = trimMessage(json, "message");
            if (json != null) {
                // show error
                displayMessage(context, json);
            } else {
                // unknown error
                if (isInternetAvailable(context)) {
                    displayMessage(context, context.getString(R.string.error_occurred_please_try_again_later));
                } else {
                    displayMessage(context, context.getString(R.string.error_please_connect_to_internet));
                }
            }
        } else if (error instanceof NoConnectionError) {
            // no internet connection
            displayMessage(context, context.getString(R.string.error_please_connect_to_internet));
        } else {
            // unknown error
            if (isInternetAvailable(context)) {
                displayMessage(context, context.getString(R.string.error_occurred_please_try_again_later));
            } else {
                displayMessage(context, context.getString(R.string.error_please_connect_to_internet));
            }
        }
    }

    public static void displayMessage(Context context, String toastString) {
        Toast.makeText(context, toastString, Toast.LENGTH_LONG).show();
    }

    public static boolean hasErrors(String[] errors) {
        if (null != errors && 0 < errors.length) {
            return true;
        }
        return false;
    }

    private static String trimMessage(String json, String key) {
        String trimmedString;
        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            Log.e(TAG, "Error while trimming message: " + e.toString(), e);
            return null;
        }
        return trimmedString;
    }

    public static boolean isInternetAvailable(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
}