package no.sevenpeakssoftware.fott.network;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface ServerRequestCallback {
    void onSuccess(JSONObject result);
    void onError(VolleyError error);
}
