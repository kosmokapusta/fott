package no.sevenpeakssoftware.fott.ui;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import no.sevenpeakssoftware.fott.R;
import no.sevenpeakssoftware.fott.network.ErrorHandler;
import no.sevenpeakssoftware.fott.network.ServerRequestCallback;
import no.sevenpeakssoftware.fott.network.ServerRequests;
import no.sevenpeakssoftware.fott.network.ServerResponseParser;

public class SplashActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ServerRequests.getAllArticlesRequest(new ServerRequestCallback() {
            @Override
            public void onSuccess(JSONObject result) {
                startMainActivity(result);
            }

            @Override
            public void onError(VolleyError error) {
                try {
                    startMainActivity(new JSONObject("{}"));
                    ErrorHandler.showError(getBaseContext(), error);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void startMainActivity(JSONObject jsonObject) {
        if (jsonObject.length() > 0) {
            ServerResponseParser.parseAllArticlesResponse(jsonObject);
        }

        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }
}
