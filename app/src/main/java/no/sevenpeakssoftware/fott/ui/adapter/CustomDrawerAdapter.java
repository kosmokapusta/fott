package no.sevenpeakssoftware.fott.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import no.sevenpeakssoftware.fott.R;
import no.sevenpeakssoftware.fott.model.DrawerItem;

public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {
    private List<DrawerItem> mItems;
    private int mItemResource;

    public CustomDrawerAdapter(Context context, int resource, List<DrawerItem> objects) {
        super(context, resource, objects);

        this.mItemResource = resource;
        this.mItems = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DrawerItemHolder holder;
        View rootView = convertView;

        if (rootView == null) {
            LayoutInflater inflater = ((Activity)getContext()).getLayoutInflater();
            holder = new DrawerItemHolder();
            rootView = inflater.inflate(mItemResource, parent, false);
            holder.title = (TextView)rootView.findViewById(R.id.section_label);
            holder.image = (ImageView)rootView.findViewById(R.id.section_image);
            rootView.setTag(holder);
        } else {
            holder = (DrawerItemHolder)rootView.getTag();
        }

        DrawerItem item = mItems.get(position);
        holder.title.setText(item.getName());
        holder.image.setImageDrawable(getContext().getResources().getDrawable(item.getImgId()));

        return rootView;
    }

    private class DrawerItemHolder {
        TextView title;
        ImageView image;
    }
}
