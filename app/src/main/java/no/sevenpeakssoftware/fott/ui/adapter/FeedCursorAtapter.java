package no.sevenpeakssoftware.fott.ui.adapter;


import android.content.Context;
import android.database.Cursor;

import android.graphics.Bitmap;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import no.sevenpeakssoftware.fott.FOTTApp;
import no.sevenpeakssoftware.fott.R;
import no.sevenpeakssoftware.fott.model.Article;
import no.sevenpeakssoftware.fott.utils.BitmapLruCache;

public class FeedCursorAtapter extends CursorAdapter {
    private static final String TAG = FeedCursorAtapter.class.getSimpleName();

    public FeedCursorAtapter(Context context, Cursor c) {
        super(context, c, false);
    }

    @Override
    public int getCount() {
        return (int)Article.count(Article.class, null, null);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.feed_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        final ViewHolder viewHolder;
        viewHolder = new ViewHolder();

        viewHolder.aticleImage = (ImageView) view.findViewById(R.id.aticleImage);
        viewHolder.articleTitle = (TextView) view.findViewById(R.id.articleTitle);
        viewHolder.articleDate = (TextView) view.findViewById(R.id.articleDate);
        viewHolder.articleIngress = (TextView) view.findViewById(R.id.articleIngress);

        view.setTag(viewHolder);

        viewHolder.requestUrl = cursor.getString(cursor.getColumnIndexOrThrow("image"));
        ImageLoader imageLoader = FOTTApp.getInstance().getImageLoader();
        BitmapLruCache bitmapLruCache = FOTTApp.getInstance().getBitmapLruCache();

        Bitmap bitmap = bitmapLruCache.get(viewHolder.requestUrl);
        if (bitmap != null) {
            viewHolder.aticleImage.setImageBitmap(bitmap);
        } else {
            viewHolder.imageLoaderContainer = imageLoader.get(viewHolder.requestUrl, new ImageLoader.ImageListener() {
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        if (viewHolder.aticleImage != null) {
                            viewHolder.aticleImage.setImageBitmap(response.getBitmap());
                            FOTTApp.getInstance().getBitmapLruCache().put(viewHolder.requestUrl, response.getBitmap());
                        }
                    }
                }

                public void onErrorResponse(VolleyError error) {
                    //TODO: set "Image unavailable" resource to aticleImage or do nothing
                }
            });
        }

        //texts
        viewHolder.articleTitle.setText(cursor.getString(cursor.getColumnIndexOrThrow("title")));
        viewHolder.articleDate.setText(cursor.getString(cursor.getColumnIndexOrThrow("date")));
        viewHolder.articleIngress.setText(cursor.getString(cursor.getColumnIndexOrThrow("ingress")));
    }

    private class ViewHolder {
        ImageView aticleImage;
        TextView articleTitle;
        TextView articleDate;
        TextView articleIngress;

        String requestUrl;
        ImageLoader.ImageContainer imageLoaderContainer;
    }
}
