package no.sevenpeakssoftware.fott.ui;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import no.sevenpeakssoftware.fott.FOTTApp;
import no.sevenpeakssoftware.fott.R;
import no.sevenpeakssoftware.fott.model.Article;
import no.sevenpeakssoftware.fott.utils.BitmapLruCache;
import no.sevenpeakssoftware.fott.utils.DbUtils;

public class DetailsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        long articleId = intent.getLongExtra("article_id", 0);
        Article currentArticle = DbUtils.getArticleById(articleId);

        getSupportActionBar().setTitle(currentArticle.getTitle());

        ImageView imageView = (ImageView) findViewById(R.id.image);
        TextView title = (TextView) findViewById(R.id.title);
        TextView date = (TextView) findViewById(R.id.date);
        TextView ingress = (TextView) findViewById(R.id.ingress);
        TextView subject = (TextView) findViewById(R.id.subject);
        TextView text = (TextView) findViewById(R.id.text);

        imageView.setImageBitmap(FOTTApp.getInstance().getBitmapLruCache().get(currentArticle.getImage()));
        title.setText(currentArticle.getTitle());
        date.setText(currentArticle.getDate());
        ingress.setText(currentArticle.getIngress());
        subject.setText(currentArticle.getSubject());
        text.setText(currentArticle.getText());
    }
}
