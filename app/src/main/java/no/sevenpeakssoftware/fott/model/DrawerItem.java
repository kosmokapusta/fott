package no.sevenpeakssoftware.fott.model;


public class DrawerItem {
    String name;
    int imgId;

    public DrawerItem(String itemName, int imgResID) {
        super();
        this.name = itemName;
        this.imgId = imgResID;
    }

    public int getImgId() {
        return this.imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
