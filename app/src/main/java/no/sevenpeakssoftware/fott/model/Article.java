package no.sevenpeakssoftware.fott.model;

import com.orm.SugarRecord;

public class Article extends SugarRecord<Article> {
    String innerId;
    int changed;
    String date;
    String title;
    String ingress;
    String text;
    String subject;
    String image;

    public Article(){
        //don't remove - orm requires
    }

    public Article(String id, String date, int changed, String title, String ingress, String text, String subject, String image) {
        this.innerId = id;
        this.date = date;
        this.changed = changed;
        this.title = title;
        this.ingress = ingress;
        this.text = text;
        this.subject = subject;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInnerId() {
        return innerId;
    }

    public void setInnerId(String id) {
        this.innerId = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String content) {
        this.text = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIngress() {
        return ingress;
    }

    public void setIngress(String ingress) {
        this.ingress = ingress;
    }

    public int getChanged() {
        return changed;
    }

    public void setChanged(int changed) {
        this.changed = changed;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
